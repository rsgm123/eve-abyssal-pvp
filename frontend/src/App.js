import Container from "@material-ui/core/Container";
import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {withStyles} from "@material-ui/core/styles";
import numeral from "numeral";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Pagination from '@material-ui/lab/Pagination';
import Box from "@material-ui/core/Box";
import ControlButton from "./ControlButton";
import CharacterRow from "./CharacterRow";

const styles = (theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  corpLogo: {
    height: '40px',
    width: '40px'
  },
});

const API_BASE = process.env.NODE_ENV === 'production' ?
    'https://bhnifczcvj.execute-api.us-east-1.amazonaws.com/prod' : '';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      leaderboards: undefined,
      leaderboard: 'isk',
      destroyed: true,
      data: [],
      page: 1,
      pages: 1,
    };

    this.selectLeaderboard = this.selectLeaderboard.bind(this);
    this.setDestroyed = this.setDestroyed.bind(this);
    this.pageChange = this.pageChange.bind(this);
  }

  fetchData(leaderboard, destroyed, page) {
    const url = this.state.leaderboards[leaderboard][destroyed ? 'destroyed' : 'lost'].url;
    fetch(`${API_BASE}${url}?page=${page}`)
        .then(result => result.json()
            .then((data) => this.setState({
              leaderboard,
              destroyed,
              data,
              page: page,
              pages: parseInt(result.headers.get('x-pages')),
            })))
  }

  getLeaderboards() {
    const [leaderboard, type] = this.state.leaderboard.split('-');
    if (this.state.leaderboards) {
      return {
        current: this.state.leaderboards[leaderboard][type],
        destroyed: this.state.leaderboards[leaderboard]['destroyed'],
        lost: this.state.leaderboards[leaderboard]['lost'],
      }
    }
  }

  selectLeaderboard(event) {
    this.fetchData(event.target.value, this.state.destroyed, 1);
  };

  setDestroyed(value) {
    this.fetchData(this.state.leaderboard, value, 1)
  };

  pageChange(event, value) {
    this.fetchData(this.state.leaderboard, this.state.destroyed, value);
  };

  statistics(x) {
    function format(x) {
      return numeral(x).format('0.00a')
    }

    const destroyed = x[this.getLeaderboards().destroyed.key];
    const lost = x[this.getLeaderboards().lost.key];
    const ratio = destroyed / lost;
    return {
      destroyed: format(destroyed),
      lost: format(lost),
      ratio: format(ratio),
    }
  }

  componentDidMount() {
    const url = API_BASE + '/api/leaderboards';
    fetch(url)
        .then(result => result.json()
            .then((data) => {
              this.setState({leaderboards: data});
              this.fetchData(this.state.leaderboard, this.state.destroyed, 1);
            }))
  }

  render() {
    const {classes} = this.props;

    function iconUrl(character) {
      const id = character.allianceId > 0 ? character.allianceId : character.corporationId;
      return `https://images.evetech.net/alliances/${id}/logo?size=64`;
    }

    return (
        <Container maxWidth="md">
          <Grid container direction="row" justify="flex-start" alignItems="stretch" spacing={1}>
            <Grid item xs={8}>
              <Box m={3} className={classes.select}>
                <h2>Abyssal PVP Leaderboard</h2>
              </Box>
            </Grid>

            <Grid item xs={4}>
              <Box m={3} className={classes.select}>
                <Select
                    labelId="Select Leaderboard"
                    id="leaderboard-select"
                    value={this.state.leaderboard.split('-')[0]}
                    onChange={this.selectLeaderboard}>
                  {Object.keys(this.state.leaderboards || {}).map((x) => (
                      <MenuItem key={x} value={x}>{x}</MenuItem>
                  ))}
                </Select>
              </Box>
            </Grid>

            {this.state.leaderboards ? (
                <Grid item xs={12}>
                  <Paper className={classes.paper} xs={12}>
                    <Grid container direction="row" alignItems="center" wrap="nowrap">
                      <Grid item xs={2}>
                        Rank
                      </Grid>
                      <Grid item xs={2}>
                        <ControlButton
                            variant="contained"
                            color={this.state.destroyed === true ? 'primary' : 'default'}
                            disableElevation
                            value={true}
                            onClick={this.setDestroyed}>
                          {this.getLeaderboards().destroyed.text}
                        </ControlButton>
                      </Grid>
                      <Grid item xs={2}>
                        <ControlButton
                            variant="contained"
                            color={this.state.destroyed === false ? 'primary' : 'default'}
                            disableElevation
                            value={false}
                            onClick={this.setDestroyed}>
                          {this.getLeaderboards().lost.text}
                        </ControlButton>
                      </Grid>
                      <Grid item xs={2}>
                        Destroyed/Lost Ratio
                      </Grid>
                      <Grid item xs={4}>
                        Character
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
            ) : (<div/>)}

            {this.state.data.map((x) => (
                <Grid item xs={12} key={x.characterId}>
                  <CharacterRow character={x} leaderboards={this.getLeaderboards()}/>
                </Grid>
            ))}

            <Grid item xs={12}>
              <Grid container justify="center">
                <Pagination size={'large'} count={this.state.pages} page={this.state.page} onChange={this.pageChange}/>
              </Grid>
            </Grid>
          </Grid>
        </Container>
    );
  }
}

export default withStyles(styles, {withTheme: true})(App);
