import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {withStyles} from "@material-ui/core/styles";
import numeral from "numeral";

const styles = (theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  corpLogo: {
    height: '40px',
    width: '40px'
  },
});

class CharacterRow extends React.Component {
  statistics() {
    function format(x) {
      return numeral(x).format('0.00a')
    }

    const destroyed = this.props.character[this.props.leaderboards.destroyed.key];
    const lost = this.props.character[this.props.leaderboards.lost.key];
    const ratio = destroyed / lost;
    return {
      destroyed: format(destroyed),
      lost: format(lost),
      ratio: format(ratio),
    }
  }

  render() {
    const {classes} = this.props;

    const id = this.props.character.allianceId > 0 ?
        this.props.character.allianceId : this.props.character.corporationId;
    const iconUrl = `https://images.evetech.net/alliances/${id}/logo?size=64`;

    return (
        <Paper className={classes.paper} xs={12}>
          <Grid container direction="row" alignItems="center" wrap="nowrap">
            <Grid item xs={2}>
              {this.props.character.rank}
            </Grid>
            <Grid item xs={2}>
              {this.statistics().destroyed}
            </Grid>
            <Grid item xs={2}>
              {this.statistics().lost}
            </Grid>
            <Grid item xs={2}>
              {this.statistics().ratio}
            </Grid>
            <Grid item xs={4}>
              <Grid container direction="row" justify="flex-start" alignItems="center">
                <Grid item xs={4}>
                  <img src={iconUrl} className={classes.corpLogo}/>
                </Grid>
                <Grid item xs={8}>
                  {this.props.character.name}
                  <br/>
                  {this.props.character.allianceId > 0 ?
                      this.props.character.allianceName : this.props.character.corporationName}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
    );
  }
}

export default withStyles(styles, {withTheme: true})(CharacterRow);
