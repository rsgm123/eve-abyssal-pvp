import Button from "@material-ui/core/Button";
import React, {Component} from "react";

export default class ControlButton extends Component {
  handleClick = () => {
    this.props.onClick(this.props.value);
  };

  render() {
    return (
        <Button
            {...this.props}
            onClick={this.handleClick}>
          {this.props.children}
        </Button>
    );
  }
}
