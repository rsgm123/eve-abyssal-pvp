from unittest.mock import Mock, patch

import pytest

from backend.app import app, HEADERS
from backend.config import LEADERBOARD_STRUCTURE
from backend.models import Character
from backend.config import CHARACTER_TABLE
from test.app_test_data import (
    LEADERBOARD_RESPONSE_DATA,
    LEADERBOARD_RESPONSE_DATA_PAGE_2,
)


@pytest.fixture
def client():
    """A test client for the app."""
    app.config["TESTING"] = True
    return app.test_client()


class TestApp:
    @staticmethod
    def assert_headers(response, expected=None):
        for k, v in {
            **HEADERS,
            **(expected or {}),
        }.items():
            assert response.headers[k] == v

    def test_health_check(self, client):
        expected = {
            "urls": ["/api/leaderboards"],
        }
        response = client.get("/api")

        assert response.status_code == 200
        assert response.json == expected
        self.assert_headers(response)

    def test_get_leaderboards(self, client):
        expected = LEADERBOARD_STRUCTURE
        response = client.get("/api/leaderboards")

        assert response.status_code == 200
        assert response.json == expected
        self.assert_headers(response)

    @patch("backend.app.batch_get")
    @patch("backend.app.redis")
    def test_get_leaderboard(self, redis, batch_get, client):
        leaderboard = "isk-destroyed"
        characters = [Character(x) for x in range(25)]
        expected = LEADERBOARD_RESPONSE_DATA

        batch_request = {
            CHARACTER_TABLE: {
                "Keys": [{"characterId": x.character_id} for x in characters[:20]],
                "ConsistentRead": False,
            }
        }

        redis.zrange.return_value = [x.character_id for x in reversed(characters)]

        resp = Mock()
        resp.get.return_value = [x.to_json_serializable() for x in characters[:20]]
        batch_get.return_value = resp

        response = client.get(f"/api/leaderboards/{leaderboard}")
        assert response.status_code == 200
        assert response.json == expected
        self.assert_headers(response, expected={"x-pages": "2"})

        redis.zrange.assert_called_once_with(
            "abyssal-pvp:leaderboard:isk_destroyed", 0, -1
        )
        resp.get.assert_called_once_with(CHARACTER_TABLE)
        batch_get.assert_called_once_with(batch_request)

    @patch("backend.app.batch_get")
    @patch("backend.app.redis")
    def test_get_leaderboard_page_2(self, redis, batch_get, client):
        leaderboard = "isk-destroyed"
        characters = [Character(x) for x in range(25)]
        expected = LEADERBOARD_RESPONSE_DATA_PAGE_2

        batch_request = {
            CHARACTER_TABLE: {
                "Keys": [{"characterId": x.character_id} for x in characters[20:25]],
                "ConsistentRead": False,
            }
        }

        redis.zrange.return_value = [x.character_id for x in reversed(characters)]

        resp = Mock()
        resp.get.return_value = [x.to_json_serializable() for x in characters[20:25]]
        batch_get.return_value = resp

        response = client.get(f"/api/leaderboards/{leaderboard}?page=2")
        assert response.status_code == 200
        assert response.json == expected
        self.assert_headers(response, expected={"x-pages": "2"})

        redis.zrange.assert_called_once_with(
            "abyssal-pvp:leaderboard:isk_destroyed", 0, -1
        )
        resp.get.assert_called_once_with(CHARACTER_TABLE)
        batch_get.assert_called_once_with(batch_request)

    def test_get_leaderboard_not_found(self, client):
        expected = {"error": "Leaderboard does not exist"}

        response = client.get("/api/leaderboards/foo")
        assert response.json == expected
        assert response.status_code == 404

    @patch("backend.app.batch_get")
    @patch("backend.app.redis")
    def test_get_leaderboard_bad_page(self, redis, batch_get, client):
        characters = [Character(x) for x in range(25)]
        expected = {"error": "Incorrect page value"}

        redis.zrange.return_value = [x.character_id for x in reversed(characters)]

        response = client.get("/api/leaderboards/isk-destroyed?page=32")
        assert response.status_code == 400
        assert response.json == expected

    @patch("backend.app.batch_get")
    @patch("backend.app.redis")
    def test_get_leaderboard_no_data(self, redis, batch_get, client):
        characters = [Character(x) for x in range(25)]
        expected = {"error": "Data not loaded"}

        redis.zrange.return_value = [x.character_id for x in reversed(characters)]
        batch_get.return_value = {}

        response = client.get("/api/leaderboards/isk-destroyed")
        assert response.status_code == 400
        assert response.json == expected

    @patch("backend.app.batch_get")
    @patch("backend.app.redis")
    def test_get_leaderboard_data_error(self, redis, batch_get, client):
        characters = [Character(x) for x in range(25)]
        expected = {"error": "Error retrieving data"}

        redis.zrange.return_value = [x.character_id for x in reversed(characters)]
        batch_get.return_value = {CHARACTER_TABLE: []}

        response = client.get("/api/leaderboards/isk-destroyed")
        assert response.status_code == 400
        assert response.json == expected

    @patch("backend.app.character_table")
    @patch("backend.app.Character")
    def test_get_character(self, character_mock, character_table, client):
        test_character = Character(12345)
        expected = test_character.to_json_serializable()
        item_data = {"characterId": test_character.character_id}

        resp = Mock()
        resp.get.return_value = item_data
        character_table.get_item.return_value = resp
        character_mock.read_item.return_value = test_character

        response = client.get(f"/api/characters/{test_character.character_id}")
        assert response.status_code == 200
        assert response.json == expected

        character_table.get_item.assert_called_once_with(Key=item_data)
        character_mock.read_item.assert_called_once_with(item_data)

    @patch("backend.app.character_table")
    def test_get_character_not_found(self, character_table, client):
        expected = {"error": "User does not exist"}

        resp = Mock()
        resp.get.return_value = None
        character_table.get_item.return_value = resp

        response = client.get("/api/characters/0")
        assert response.json == expected
        assert response.status_code == 404
