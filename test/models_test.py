from collections import Counter
from decimal import Decimal
from unittest.mock import Mock, patch

from backend.config import ESI_HEADERS, ESI_BASE
from backend.models import Character
from test.models_test_data import (
    TEST_CHARACTER_DATA,
    TEST_CHARACTER_ITEM,
    TEST_CHARACTER_JSON,
)


class TestCharacter:
    def create_character(self):
        character = Character(123)
        for k, v in TEST_CHARACTER_DATA.items():
            if k == "ships_used":
                character.ships_used = Counter(v)
                continue
            if k == "top_ships":
                continue
            setattr(character, k, v)

        return character

    def test_instance(self):
        expected = 5

        character = Character(expected)
        assert character.character_id == expected

    @patch("backend.models.get_and_retry")
    def test_lookup_name(self, get):
        character_id = 90610935
        character_name = "Test Character"
        corporation_id = 98356193
        corporation_name = "Corporation"
        alliance_id = 434243723
        alliance_name = "Alliance"

        character = Character(character_id)
        character.corporation_id = corporation_id
        character.alliance_id = alliance_id

        response = Mock()
        response.json.return_value = [
            {"id": character_id, "name": character_name},
            {"id": corporation_id, "name": corporation_name},
            {"id": alliance_id, "name": alliance_name},
        ]
        get.return_value = response

        character.lookup_name()
        assert character.name == character_name
        assert character.corporation_name == corporation_name
        assert character.alliance_name == alliance_name

        get.assert_called_once_with(
            f"{ESI_BASE}/v3/universe/names/",
            ESI_HEADERS,
            post_data=[
                character_id,
                corporation_id,
                alliance_id,
            ],
            raise_error=False,
        )
        response.json.assert_called_once()

    @patch("backend.models.get_and_retry")
    def test_lookup_name_error(self, get):
        character_id = 5

        character = Character(character_id)

        get.return_value = None

        character.lookup_name()
        assert character.name == ""

        get.assert_called_once_with(
            f"{ESI_BASE}/v3/universe/names/",
            ESI_HEADERS,
            post_data=[
                character_id,
                character.corporation_id,
            ],
            raise_error=False,
        )

    def test_add_ship(self):
        character = Character(1)

        character.add_ship(780)
        character.add_ship(360)
        character.add_ship(780)

        assert character.ships_used[360] == 1
        assert character.ships_used[780] == 2

    def test_update_as_victim(self):
        character = Character(1)
        character.fitted_isk_lost = Decimal(50000)
        character.isk_lost = Decimal(600000)
        character.points_lost = Decimal(90)
        character.ships_lost = Decimal(32)

        character.update_as_victim(10000, 100000, 10)

        assert character.fitted_isk_lost == Decimal(60000)
        assert character.isk_lost == Decimal(700000)
        assert character.points_lost == Decimal(100)
        assert character.ships_lost == Decimal(33)

    def test_update_as_attacker(self):
        character = Character(1)
        character.damage_done = Decimal(50000)
        character.isk_destroyed = Decimal(600000)
        character.points_destroyed = Decimal(90)

        character.update_as_attacker(10000, 100000, 10)

        assert character.damage_done == Decimal(60000)
        assert character.isk_destroyed == Decimal(700000)
        assert character.points_destroyed == Decimal(100)

    def test_read_item(self):
        expected = TEST_CHARACTER_DATA
        # TEST_CHARACTER_ITEM here is what would be returned from dynamodb, which follows camel case
        character = Character.read_item(TEST_CHARACTER_ITEM)

        for k, v in expected.items():
            # Some integers are converted into Decimal objects
            # shipsUsed is also converted into a counter from a dict, however dict(x) == Counter(x)
            assert getattr(character, k) == v or getattr(character, k) == Decimal(v)

    @patch("backend.models.character_table")
    def test_save(self, character_table):
        expected = TEST_CHARACTER_ITEM

        character = self.create_character()

        character.save()
        character_table.put_item.assert_called_once_with(Item=expected)

        batch = Mock()
        character.save(batch=batch)
        batch.put_item.assert_called_once_with(Item=expected)

    def test_to_json_serializable(self):
        expected = TEST_CHARACTER_JSON
        character = self.create_character()
        assert character.to_json_serializable() == expected
