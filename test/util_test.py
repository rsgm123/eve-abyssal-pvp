from unittest.mock import Mock, patch

import pytest
from requests import HTTPError

from backend.config import ESI_HEADERS, ESI_BASE
from backend.util import chunks, get_and_retry, batch_get


class TestChunks:
    def test_empty_list(self):
        expected = []

        result = []
        for x in chunks([], 1):
            result.append(x)

        assert result == expected

    def test_normal_list(self):
        expected = [[1, 2], [3]]

        result = []
        for x in chunks([1, 2, 3], 2):
            result.append(x)

        assert result == expected


class TestBatchGet:
    @patch("backend.util.dynamodb")
    def test_success(self, dynamodb):
        expected = {"character-table-dev": [{"characterId": x} for x in range(5)]}

        dynamodb.batch_get_item.return_value = {
            "Responses": {
                "character-table-dev": [{"characterId": x} for x in range(5)],
            },
            "UnprocessedKeys": {},
            "ConsumedCapacity": {},
        }

        requested_items = {
            "character-table-dev": {
                "Keys": [{"characterId": x} for x in range(5)],
                "ConsistentRead": False,
            }
        }

        assert expected == batch_get(requested_items)
        dynamodb.batch_get_item.assert_called_once_with(
            RequestItems=requested_items, ReturnConsumedCapacity="INDEXES"
        )

    @patch("backend.util.dynamodb")
    def test_success_unprocessed_keys(self, dynamodb):
        expected = {"character-table-dev": [{"characterId": x} for x in range(10)]}

        requested_items = {
            "character-table-dev": {
                "Keys": [{"characterId": x} for x in range(10)],
                "ConsistentRead": False,
            }
        }
        unprocessed_keys = {
            "character-table-dev": {
                "Keys": [{"characterId": x + 5} for x in range(5)],
                "ConsistentRead": False,
            }
        }

        dynamodb.batch_get_item.side_effect = [
            {
                "Responses": {
                    "character-table-dev": [{"characterId": x} for x in range(5)],
                },
                "UnprocessedKeys": unprocessed_keys,
                "ConsumedCapacity": {},
            },
            {
                "Responses": {
                    "character-table-dev": [{"characterId": x + 5} for x in range(5)],
                },
                "UnprocessedKeys": {},
                "ConsumedCapacity": {},
            },
        ]

        assert expected == batch_get(requested_items)
        dynamodb.batch_get_item.assert_any_call(
            RequestItems=requested_items, ReturnConsumedCapacity="INDEXES"
        )
        dynamodb.batch_get_item.assert_called_with(
            RequestItems=unprocessed_keys, ReturnConsumedCapacity="INDEXES"
        )
        assert dynamodb.batch_get_item.call_count == 2


class TestGetAndRetry:
    @patch("backend.util.SESSION")
    def test_success(self, requests):
        result = Mock()
        requests.get.return_value = result

        assert result == get_and_retry(ESI_BASE, ESI_HEADERS)
        requests.get.assert_called_once_with(ESI_BASE, headers=ESI_HEADERS)
        result.raise_for_status.assert_called_once()

    @patch("backend.util.time")
    @patch("backend.util.SESSION")
    def test_one_exceptions_success(self, requests, time):
        result = Mock()
        requests.get.return_value = result
        result.raise_for_status.side_effect = [HTTPError(), None]

        assert result == get_and_retry(ESI_BASE, ESI_HEADERS)
        requests.get.assert_called_with(ESI_BASE, headers=ESI_HEADERS)
        result.raise_for_status.assert_called()
        time.sleep.assert_called_once()

    @patch("backend.util.time")
    @patch("backend.util.SESSION")
    def test_three_exceptions(self, requests, time):
        result = Mock()
        requests.get.return_value = result
        result.raise_for_status.side_effect = HTTPError()

        with pytest.raises(HTTPError):
            assert get_and_retry(ESI_BASE, ESI_HEADERS)

        assert requests.get.call_count == 3

    @patch("backend.util.time")
    @patch("backend.util.SESSION")
    def test_three_exceptions_and_return(self, requests, time):
        expected = None
        result = Mock()
        requests.get.return_value = result
        result.raise_for_status.side_effect = HTTPError()

        assert get_and_retry(ESI_BASE, ESI_HEADERS, raise_error=False) is expected
        assert requests.get.call_count == 3
