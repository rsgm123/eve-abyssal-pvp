from decimal import Decimal

# character keys and values as
TEST_CHARACTER_DATA = {
    "character_id": 123,
    "name": "Jake",
    "alliance_id": 34253754,
    "alliance_name": "Test Alliance Please ignore",
    "corporation_id": 5645768,
    "corporation_name": "Test Corp",
    "ships_used": {
        593: 10,
        34828: 3,
        11379: 6,
        37457: 21,
    },
    "top_ships": [
        {"typeId": 37457, "name": "Deacon", "usage": 0.525},
        {"typeId": 593, "name": "Tristan", "usage": 0.25},
        {"typeId": 11379, "name": "Hawk", "usage": 0.15},
        {"typeId": 34828, "name": "Jackdaw", "usage": 0.075},
    ],
    "ships_lost": 654,
    "ships_destroyed": 2,
    "damage_done": 10000,
    "fitted_isk_lost": 10000,
    "isk_destroyed": 10000,
    "isk_lost": 10000,
    "points_destroyed": 100,
    "points_lost": 62,
}

# dynamodb data is formatted as camel case like json,
# except using Decimal objects for some attributes
# and missing the top_ships property
TEST_CHARACTER_ITEM = {
    "characterId": 123.0,
    "name": "Jake",
    "allianceName": "Test Alliance Please ignore",
    "allianceId": 34253754,
    "corporationId": 5645768,
    "corporationName": "Test Corp",
    "shipsUsed": {
        "593": Decimal("10"),
        "34828": Decimal("3"),
        "11379": Decimal("6"),
        "37457": Decimal("21"),
    },
    "shipsLost": Decimal(654),
    "shipsDestroyed": Decimal(2),
    "damageDone": Decimal(10000),
    "fittedIskLost": Decimal(10000),
    "iskDestroyed": Decimal(10000),
    "iskLost": Decimal(10000),
    "pointsDestroyed": Decimal(100),
    "pointsLost": Decimal(62),
}

# json is formatted as camel case
TEST_CHARACTER_JSON = {
    "".join([k.split("_")[0]] + [x.title() for x in k.split("_")[1:]]): v
    for k, v in TEST_CHARACTER_DATA.items()
}
