# [Abyssal PVP Leaderboards](https://abyssal-pvp.com)

![pipeline](https://gitlab.com/rsgm123/eve-abyssal-pvp/badges/master/pipeline.svg)
![coverage](https://gitlab.com/rsgm123/eve-abyssal-pvp/badges/master/coverage.svg)

A public leaderboard for Abyssal PVP in EVE Online, aggregating various statistics.


## Design

Abyssal-pvp was built with serveless in mind. The frontend is a react app, which is hosted in S3.
The backend is a python flask API, along with some batch functions, hosted on AWS Lambda,
using dynamoBD and Upstash(formerly lambdastore). It uses the serverless framework to deploy the project.

Here is a more visual description.  
![aws-design](https://gitlab.com/rsgm123/eve-abyssal-pvp/-/raw/master/abyssal-pvp-aws-design.svg)
