import json
from decimal import Decimal

from requests import HTTPError

from .config import (
    LEADERBOARDS,
    character_table,
    ESI_BASE,
    ESI_HEADERS,
    ZKILL_BASE,
    ZKILL_HEADERS,
    CHARACTER_TABLE,
    redis,
)
from .models import Character
from .util import get_and_retry, chunks, batch_get

REDIS_NEWEST_KILLMAIL_ID = "abyssal-pvp:update:last_killmail_id"


def fetch_killmails(kill_id, kill_hash):
    esi_killmail = get_and_retry(
        f"{ESI_BASE}/v1/killmails/{kill_id}/{kill_hash}/", headers=ESI_HEADERS
    ).json()
    zkillmail = get_and_retry(
        f"{ZKILL_BASE}/killID/{kill_id}/", headers=ZKILL_HEADERS
    ).json()[0]

    return esi_killmail, zkillmail


def fetch_characters(character_ids):
    characters = {}
    character_ids = list(set(character_ids))
    for chunk in chunks(character_ids, 80):
        response = batch_get(
            {
                CHARACTER_TABLE: {
                    "Keys": [{"characterId": x} for x in chunk],
                    "ConsistentRead": False,
                }
            }
        )

        for x in response.get(CHARACTER_TABLE, []):
            character = Character.read_item(x)
            characters[character.character_id] = character

    return characters


def get_or_create_character(character, characters):
    character_id = character["character_id"]
    if character_id not in characters:
        new_character = Character(character_id)
        new_character.corporation_id = character["corporation_id"]
        new_character.alliance_id = character.get("alliance_id", -1)
        new_character.lookup_name()

        characters[character_id] = new_character

    return characters[character_id]


def update_leaderboards(characters):
    if len(characters) > 0:
        for leaderboard in LEADERBOARDS.values():
            redis.zadd(
                leaderboard["redis_key"],
                {
                    x.character_id: x.to_json_serializable()[leaderboard["key"]]
                    for x in characters
                },
            )


def abyssal_update_handler(*args):
    """
    Lambda function to fetch abyssal killmail data and invoke individual abyssal_update_handler functions.
    """
    last_killmail_id = redis.get(REDIS_NEWEST_KILLMAIL_ID) or 0

    if last_killmail_id != 0:
        last_killmail_id = int(last_killmail_id.decode("utf-8"))

    characters = {}

    def finish_update():
        print("saving update data")
        update_leaderboards(characters.values())
        with character_table.batch_writer() as batch:
            for character in characters.values():
                character.save(batch=batch)

    # zkill only gives 20 pages of results
    # but we can only do about 5 pages within the 15 minute function timeout
    for p in range(5):
        page = p + 1
        print(f"page: {page}")
        try:
            killmails = get_and_retry(
                f"{ZKILL_BASE}/kills/abyssalpvp/page/{page}/", headers=ZKILL_HEADERS
            ).json()

        except HTTPError as ex:
            print(f"error fetching zkill page {page}")
            print(ex)

            # if this is the first page, we'll just try again next update
            if page == 1:
                raise ex

            # otherwise, we can just ignore the page
            continue

        # sort killmails so we can easily get the highest ID and iterate through them in order
        killmails = map(lambda x: (int(x[0]), x[1]), killmails.items())
        killmails = sorted(killmails, key=lambda x: x[0], reverse=True)

        # store newest killmail ID for later storage
        if page == 1:
            redis.set(REDIS_NEWEST_KILLMAIL_ID, killmails[0][0])

        # run updates for this page of killmails
        for i, x in enumerate(killmails):
            k, v = x
            print(f"{i}/{len(killmails)} - {k}")

            # if we hit a previous killmail ID, stop fetching pages, update leaderboards and return
            if k <= last_killmail_id:
                print("found old killmaill, stopping")
                finish_update()
                return

            # run update
            update_killmail(k, v, characters)

    finish_update()


def update_killmail(kill_id, kill_hash, characters):
    # fetch killmail data
    try:
        esi_killmail, zkillmail = fetch_killmails(kill_id, kill_hash)

    except HTTPError as ex:
        print(f"error fetching killmail ({kill_id}, {kill_hash})")
        print(ex)

        # ignore the error
        return

    # batch fetch characters
    character_ids = {
        x["character_id"] for x in esi_killmail["attackers"] if "character_id" in x
    }
    character_ids.add(esi_killmail["victim"]["character_id"])

    # remove IDs of characters we already have
    character_ids = character_ids - set(characters.keys())
    characters.update(fetch_characters(character_ids))

    victim = esi_killmail["victim"]
    attackers = esi_killmail["attackers"]

    # get shared stats
    fitted_isk = Decimal(zkillmail["zkb"]["fittedValue"])
    isk = Decimal(zkillmail["zkb"]["totalValue"])
    points = Decimal(zkillmail["zkb"]["points"])

    # victim first
    character = get_or_create_character(victim, characters)
    character.add_ship(victim["ship_type_id"])
    character.update_as_victim(fitted_isk, isk, points)

    # now attackers
    for attacker in attackers:
        if "character_id" in attacker:
            character = get_or_create_character(attacker, characters)
            character.update_as_attacker(Decimal(attacker["damage_done"]), isk, points)

            if "ship_type_id" in attacker:
                character.add_ship(attacker["ship_type_id"])


def recalculate_leaderboards_handler(*args):
    characters = character_table.scan()["Items"]
    characters = [Character.read_item(x) for x in characters]

    # clear leaderboards
    redis.delete(*[x["redis_key"] for x in LEADERBOARDS.values()])

    update_leaderboards(characters)


def load_data_handler(*args):
    redis.flushall()

    with open("dynamodb_data.json") as file:
        data = json.load(file)
        characters = [Character.read_item(x) for x in data]

        with character_table.batch_writer() as batch:
            for x in characters:
                x.save(batch=batch)

    recalculate_leaderboards_handler(None, None)


def create_static_data():
    load_data_handler("", "")
    abyssal_update_handler("", "")

    # transform data
    data = character_table.scan()["Items"]
    data = [Character.read_item(x).to_json_serializable() for x in data]
    with open("dynamodb_data.json", mode="w") as file:
        json.dump(data, file)


def fetch_names_handler(*args):
    """
    Lambda function to update every character with corp and alliance names.
    """
    characters = [Character.read_item(x) for x in character_table.scan()["Items"]]
    with character_table.batch_writer() as batch:
        for chunk in chunks(characters, 300):
            data = {int(x.corporation_id) for x in characters}
            data.update({int(x.alliance_id) for x in characters if x.alliance_id > 0})

            r = get_and_retry(
                f"{ESI_BASE}/v3/universe/names/",
                ESI_HEADERS,
                post_data=list(data),
                raise_error=False,
            )

            if r:
                data = {x["id"]: x["name"] for x in r.json()}

                for x in chunk:
                    x.corporation_name = data.get(x.corporation_id, x.corporation_name)
                    x.alliance_name = data.get(x.alliance_id, x.alliance_name)
                    x.save(batch=batch)
