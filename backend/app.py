import math

from flask import Flask, jsonify
from flask import request

from .config import (
    LEADERBOARDS,
    LEADERBOARD_STRUCTURE,
    CHARACTER_TABLE,
    character_table,
    redis,
)
from .models import Character
from .util import batch_get

HEADERS = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Expose-Headers": "X-Pages",
}

PAGE_SIZE = 20

app = Flask(__name__)


@app.after_request
def default_headers(response):
    response.headers = {
        **HEADERS,
        **response.headers,
    }
    return response


@app.route("/api")
def health_check():
    return {
        "urls": ["/api/leaderboards"],
    }


@app.route("/api/leaderboards")
def get_leaderboards():
    return jsonify(LEADERBOARD_STRUCTURE)


@app.route("/api/leaderboards/<string:leaderboard>")
def get_leaderboard(leaderboard):
    key = LEADERBOARDS.get(leaderboard)
    if not key:
        return jsonify({"error": "Leaderboard does not exist"}), 404

    character_ids = [int(x) for x in reversed(redis.zrange(key["redis_key"], 0, -1))]

    page = request.args.get("page", 1, type=int) - 1
    pages = math.ceil(len(character_ids) / PAGE_SIZE)
    if page < 0 or page * PAGE_SIZE > len(character_ids):
        return jsonify({"error": "Incorrect page value"}), 400

    # get slice of character_ids for the requested page
    character_ids = character_ids[page * PAGE_SIZE : page * PAGE_SIZE + PAGE_SIZE]

    requested_items = {
        CHARACTER_TABLE: {
            "Keys": [{"characterId": x} for x in character_ids],
            "ConsistentRead": False,
        }
    }

    characters = batch_get(requested_items)
    if not characters:
        return jsonify({"error": "Data not loaded"}), 400

    characters = characters.get(CHARACTER_TABLE)
    if not characters:
        return jsonify({"error": "Error retrieving data"}), 400

    # read data
    characters = [Character.read_item(x).to_json_serializable() for x in characters]
    # transform data into dict
    characters = {x["characterId"]: x for x in characters}
    # order character objects by returned character_ids, and add rank
    characters = [
        {**characters[x], "rank": i + page * PAGE_SIZE + 1}
        for i, x in enumerate(character_ids)
    ]

    response = jsonify(characters)
    response.headers["X-Pages"] = pages
    return response


@app.route("/api/characters/<int:character_id>")
def get_character(character_id):
    resp = character_table.get_item(Key={"characterId": character_id})
    item = resp.get("Item")

    if not item:
        return jsonify({"error": "User does not exist"}), 404

    return jsonify(Character.read_item(item).to_json_serializable())
