import itertools
import json
import os

import boto3
from redis import StrictRedis

CHARACTER_TABLE = os.environ.get("character_table", "character-table-dev")

ESI_BASE = "https://esi.evetech.net"
ZKILL_BASE = "https://zkillboard.com/api"
ESI_HEADERS = {
    "User-Agent": "abyssal-pvp-leaderboard, by rsgm vaille - rsgm.eve@gmail.com",
}
ZKILL_HEADERS = {
    "Accept-Encoding": "gzip",
    "User-Agent": "abyssal-pvp-leaderboard, by rsgm vaille - rsgm.eve@gmail.com",
}

# redis keys
LEADERBOARD_ISK_DESTROYED = "abyssal-pvp:leaderboard:isk_destroyed"
LEADERBOARD_ISK_LOST = "abyssal-pvp:leaderboard:isk_lost"
LEADERBOARD_POINTS_DESTROYED = "abyssal-pvp:leaderboard:points_destroyed"
LEADERBOARD_POINTS_LOST = "abyssal-pvp:leaderboard:points_lost"
LEADERBOARD_SHIPS_DESTROYED = "abyssal-pvp:leaderboard:ships_destroyed"
LEADERBOARD_SHIPS_LOST = "abyssal-pvp:leaderboard:ships_lost"
LAST_ZKILL = "abyssal-pvp:zkill:last"


# leaderboards
LEADERBOARDS = {
    "isk-destroyed": {
        "url": "/api/leaderboards/isk-destroyed",
        "key": "iskDestroyed",
        "redis_key": LEADERBOARD_ISK_DESTROYED,
        "text": "Isk Destroyed",
    },
    "isk-lost": {
        "url": "/api/leaderboards/isk-lost",
        "key": "iskLost",
        "redis_key": LEADERBOARD_ISK_LOST,
        "text": "Isk Lost",
    },
    "points-destroyed": {
        "url": "/api/leaderboards/points-destroyed",
        "key": "pointsDestroyed",
        "redis_key": LEADERBOARD_POINTS_DESTROYED,
        "text": "Points Destroyed",
    },
    "points-lost": {
        "url": "/api/leaderboards/points-lost",
        "key": "pointsLost",
        "redis_key": LEADERBOARD_POINTS_LOST,
        "text": "Points Lost",
    },
    "ships-destroyed": {
        "url": "/api/leaderboards/ships-destroyed",
        "key": "shipsDestroyed",
        "redis_key": LEADERBOARD_SHIPS_DESTROYED,
        "text": "Ships Destroyed",
    },
    "ships-lost": {
        "url": "/api/leaderboards/ships-lost",
        "key": "shipsLost",
        "redis_key": LEADERBOARD_SHIPS_LOST,
        "text": "Ships Lost",
    },
}
LEADERBOARD_STRUCTURE = {
    "isk": {
        "destroyed": LEADERBOARDS["isk-destroyed"],
        "lost": LEADERBOARDS["isk-lost"],
    },
    "points": {
        "destroyed": LEADERBOARDS["points-destroyed"],
        "lost": LEADERBOARDS["points-lost"],
    },
    "ships": {
        "destroyed": LEADERBOARDS["ships-destroyed"],
        "lost": LEADERBOARDS["ships-lost"],
    },
}

with open("ship_type_names.json") as file:
    SHIP_NAMES = json.load(file)

redis = StrictRedis()
if os.environ.get("redis_host"):
    redis = StrictRedis(
        host=os.environ.get("redis_host"),
        port=os.environ.get("redis_port"),
        password=os.environ.get("redis_password"),
        ssl=True,
    )

dynamodb_endpoint = None
if os.environ.get("stage", "dev") == "dev":
    dynamodb_endpoint = "http://localhost:8000"


# don't try to connect to dynamo during testing
# this only gives errors in gitlab cicd
dynamodb = None
character_table = None
if os.environ.get("stage"):
    dynamodb = boto3.resource("dynamodb", endpoint_url=dynamodb_endpoint)
    character_table = dynamodb.Table(CHARACTER_TABLE)
