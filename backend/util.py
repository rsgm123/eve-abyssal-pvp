import time
from collections import defaultdict

import requests
from requests import HTTPError

from .config import dynamodb, ZKILL_BASE

SESSION = requests.Session()

ZKILL_RATE_LIMIT = 1
last_zkill_call = time.time()


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def batch_get(requested_items):
    """
    Batch dynamodb get items
    """
    result = defaultdict(list)
    while len(requested_items) > 0:
        response = dynamodb.batch_get_item(
            RequestItems=requested_items, ReturnConsumedCapacity="INDEXES"
        )

        if response["UnprocessedKeys"]:
            print(f"batch_get unprocessed keys: {response['UnprocessedKeys']}")
        # print(f"batch_get consumed capacity: {response['ConsumedCapacity']}")

        requested_items = response["UnprocessedKeys"]
        for table, values in response["Responses"].items():
            result[table] += values

    return result


def get_and_retry(url, headers, post_data=None, raise_error=True):
    """
    Make an http request, retry up to 3 times on errors.
    :param url: request url
    :param headers: request headers
    :param raise_error: raise or ignore last error
    :return: response
    """
    global last_zkill_call

    # ratelimit zkillboard
    last_zkill_time = time.time() - last_zkill_call
    if url.startswith(ZKILL_BASE) and last_zkill_time < ZKILL_RATE_LIMIT:
        time.sleep(ZKILL_RATE_LIMIT - last_zkill_time)  # sleep for time difference
        last_zkill_call = time.time()

    last_exception = None
    for x in range(3):
        if post_data:
            r = SESSION.post(url, json=post_data, headers=headers)
        else:
            r = SESSION.get(url, headers=headers)

        try:
            r.raise_for_status()
        except HTTPError as ex:
            last_exception = ex
            print(ex)

            # I hate this, but the zkill api is not meant for high throughput
            # and esi is flaky at times
            time.sleep(1)
            continue

        if url.startswith(ZKILL_BASE):
            last_zkill_call = time.time()

        return r

    if last_exception and raise_error:
        raise last_exception
