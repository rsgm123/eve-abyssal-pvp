from collections import Counter
from decimal import Decimal

from .config import character_table, ESI_BASE, ESI_HEADERS, SHIP_NAMES
from .util import get_and_retry


class Character:
    character_id = None

    name = ""
    alliance_id = -1
    alliance_name = ""
    corporation_id = -1
    corporation_name = ""

    ships_used = None
    ships_lost = Decimal(0)
    ships_destroyed = Decimal(0)

    damage_done = Decimal(0)
    fitted_isk_lost = Decimal(0)

    isk_destroyed = Decimal(0)
    isk_lost = Decimal(0)
    points_destroyed = Decimal(0)
    points_lost = Decimal(0)

    def __init__(self, character_id):
        self.character_id = character_id
        self.ships_used = Counter()

    @property
    def top_ships(self):
        total = sum(self.ships_used.values())
        return [
            {
                "typeId": x,
                "name": SHIP_NAMES.get(str(x), f"{x}(Unknown)"),
                "usage": count / total,
            }
            for x, count in self.ships_used.most_common()[:5]
        ]

    def lookup_name(self):
        """
        Lookup the character name using the ESI.
        """

        data = [
            int(self.character_id),
            int(self.corporation_id),
        ]
        if self.alliance_id > 0:
            data.append(int(self.alliance_id))

        r = get_and_retry(
            f"{ESI_BASE}/v3/universe/names/",
            ESI_HEADERS,
            post_data=data,
            raise_error=False,
        )

        if r:
            data = {x["id"]: x["name"] for x in r.json()}
            self.name = data.get(self.character_id, self.name)
            self.corporation_name = data.get(self.corporation_id, self.corporation_name)
            self.alliance_name = data.get(self.alliance_id, self.alliance_name)

    def add_ship(self, ship):
        """
        Adds a shiptype to the used ships attribute, which holds number of times used.
        """
        self.ships_used.update([ship])

    def update_as_victim(self, fitted_isk, isk, points):
        """
        Updates character data as an attacker on a victim.
        """
        self.fitted_isk_lost += fitted_isk
        self.isk_lost += isk
        self.points_lost += points
        self.ships_lost += 1

    def update_as_attacker(self, damage_done, isk, points):
        """
        Updates character data as an attacker on a killmail.
        """
        self.damage_done += damage_done
        self.isk_destroyed += isk
        self.points_destroyed += points
        self.ships_destroyed += 1

    @staticmethod
    def read_item(character_item):
        """
        Reads a dynamodb response into a character object.
        """
        character = Character(int(character_item["characterId"]))

        character.name = character_item["name"]
        character.alliance_id = character_item["allianceId"]
        character.alliance_name = character_item.get("allianceName", "")
        character.corporation_id = character_item["corporationId"]
        character.corporation_name = character_item.get("corporationName", "")

        character.ships_used = Counter(
            {int(k): int(v) for k, v in character_item["shipsUsed"].items()}
        )
        character.ships_lost = int(character_item["shipsLost"])
        character.ships_destroyed = int(character_item.get("shipsDestroyed", 0))
        character.damage_done = int(character_item["damageDone"])
        character.fitted_isk_lost = int(character_item["fittedIskLost"])

        character.isk_destroyed = int(character_item["iskDestroyed"])
        character.isk_lost = int(character_item["iskLost"])
        character.points_destroyed = int(character_item["pointsDestroyed"])
        character.points_lost = int(character_item["pointsLost"])

        return character

    def save(self, batch=None):
        """
        Saves character to dynamodb, using batch if available
        """
        character = {
            "characterId": self.character_id,
            "name": self.name,
            "allianceId": self.alliance_id,
            "allianceName": self.alliance_name,
            "corporationId": self.corporation_id,
            "corporationName": self.corporation_name,
            "shipsUsed": {str(k): v for k, v in self.ships_used.items()},
            "shipsLost": self.ships_lost,
            "shipsDestroyed": self.ships_destroyed,
            "damageDone": self.damage_done,
            "fittedIskLost": self.fitted_isk_lost,
            "iskDestroyed": self.isk_destroyed,
            "iskLost": self.isk_lost,
            "pointsDestroyed": self.points_destroyed,
            "pointsLost": self.points_lost,
        }

        if batch:
            return batch.put_item(Item=character)

        return character_table.put_item(Item=character)

    def to_json_serializable(self):
        """
        Transforms character into a json serializable dict.
        """
        return {
            "characterId": self.character_id,
            "name": self.name,
            "allianceId": int(self.alliance_id),
            "allianceName": self.alliance_name,
            "corporationId": int(self.corporation_id),
            "corporationName": self.corporation_name,
            "shipsUsed": {int(k): int(v) for k, v in self.ships_used.items()},
            "topShips": self.top_ships,
            "shipsLost": int(self.ships_lost),
            "shipsDestroyed": int(self.ships_destroyed),
            "damageDone": int(self.damage_done),
            "fittedIskLost": int(self.fitted_isk_lost),
            "iskDestroyed": int(self.isk_destroyed),
            "iskLost": int(self.isk_lost),
            "pointsDestroyed": int(self.points_destroyed),
            "pointsLost": int(self.points_lost),
        }
